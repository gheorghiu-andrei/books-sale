import { DiscountLevel } from "@/types";

export const numberFormat = (
  number: number,
  options: Intl.NumberFormatOptions = { style: 'currency', currency: 'USD' },
  locales = 'en-US'
): string => new Intl.NumberFormat(locales, options).format(number);

export const discountProduct = (level: DiscountLevel): number => level.count * level.value;

export const makeDiscountLevels = (levels: number[]): DiscountLevel[] => levels
  .map((discount, index) => discount * (index + 1))
  .map((value, index, list) => ({
    count: index + 1,
    value,
    upgrades: list
      .slice(index, list.length)
      .map((n, i, l) => l[i + 1] - n)
      .slice(0, list.length - index - 1)
      .map((n, i, l) =>
        l.slice(0, i + 1).reduce((a, b) => a + b, 0)),
    downgrades: list
      .slice(0, index + 1)
      .map((n, i, l) => n - l[i - 1])
      .slice(1, index + 1)
      .reverse()
      .map((n, i, l) =>
        l.slice(0, i + 1).reduce((a, b) => a + b, 0))
  }));
