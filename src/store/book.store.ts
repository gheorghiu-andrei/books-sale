import { $store } from "@/main";
import { Module, VuexModule } from "vuex-module-decorators";
import { v4 } from 'uuid';
import { BookRecord } from "@/types";

@Module({ namespaced: true, store: $store, name: 'book' })
export default class BookStore extends VuexModule {
  books: BookRecord[] = Array.from(Array(5).keys()).map(key => ({
    title: `Book ${key + 1}`,
    price: 8,
    id: v4()
  }));
}
