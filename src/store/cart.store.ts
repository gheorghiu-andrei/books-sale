import { $store } from "@/main";
import { Module, Mutation, VuexModule } from "vuex-module-decorators";
import { BookRecord, CartItem, DiscountGroup } from "@/types";
import { orderBy, times, isEqual } from "lodash-es";
import { makeDiscountLevels } from "@/helper";

@Module({ namespaced: true, store: $store, name: 'cart' })
export default class CartStore extends VuexModule {
  items: CartItem[] = [];
  discounts = [0, 5, 10, 20, 25];

  get total(): number {
    return this.items
      .map(({ price, qty }) => price * qty)
      .reduce((a, b) => a + b, 0);
  }

  get count(): number {
    return this.items.length;
  }

  get sortedItems(): CartItem[] {
    return orderBy(this.items, ['qty', 'price'], ['desc', 'desc']).map(
      item => ({ ...item, sold: 0 })
    );
  }

  get discountLevels(): any[] {
    return makeDiscountLevels(this.discounts);
  }

  get discountGroups(): DiscountGroup[] {
    const generator = this.sortedItems.reduce((acc, item, index) => {
      while (this.sortedItems.filter(({ qty, id }) => qty > acc.stocks[id]).length) {
        const group = this.sortedItems
          .filter(({ qty, id }) => qty > acc.stocks[id])
          .slice(index, index + this.discounts.length);
        const multiplier = group[group.length - 1].qty - acc.stocks[group[group.length - 1].id];
        acc.groups.push({
          group: group.map(({ id }) => id),
          multiplier
        });
        group.forEach(item => {
          acc.stocks[item.id] += multiplier;
        });
      }
      return acc;
    }, {
      groups: [] as DiscountGroup[],
      stocks: Object.assign({}, ...this.items.map(({ id }) => ({ [id]: 0 })))
    }).groups;
    const groups = generator
      .map(g => times(g.multiplier, () => g.group))
      .flat();
    let baseGroup: string[] | undefined = groups[0];
    let { length } = groups[0] || [];

    // level 1
    while(length > 1) {
      const targetGroup: string[] | undefined = groups.find(
        g => g.length < length && baseGroup &&
          this.discountLevels[g.length - 1].upgrades[0] > this.discountLevels[baseGroup.length - 1].downgrades[0]
      );
      if (baseGroup && targetGroup) {
        const index = baseGroup.findIndex(id => !targetGroup.includes(id));
        targetGroup.push(...baseGroup.splice(index, 1));
      } else {
        length = length - 1;
      }
      baseGroup = groups.find(g => g.length === length);
    }

    // toDo: level 2 !?

    // regroup
    return groups
      .map(group => ({
        group,
        discount: this.discounts[group.length - 1],
        multiplier: 1
      }))
      .filter(({ discount }) => discount)
      .reduce((acc, item) => {
        const index = acc.findIndex(g => isEqual(g.group, item.group));
        if (index > -1) {
          acc[index].multiplier += 1;
        } else {
          acc.push(item);
        }
        return acc;
      }, [] as DiscountGroup[]) || [];
  }

  get discountedTotal(): number {
    return this.discountGroups.reduce((acc, group) => {
      acc.total += group.group.map(id => {
        const item = this.items.find(item => item.id === id);
        return item
          ? item.price * group.multiplier *
          (1 - (this.discounts[group.group.length - 1] || 0) / 100)
          : 0;
      }).reduce((a, b) => a + b, 0);
      return acc;
    }, { total: 0 }).total || 0;
  }

  @Mutation
  addToCart(book: BookRecord): void {
    const index = this.items.findIndex(({ title }) => title === book.title);
    if (index < 0) {
      this.items.push({ ...book, qty: 1 });
    } else {
      const item = this.items[index];
      this.items.splice(index, 1, { ...item, qty: item.qty + 1 });
    }
  }

  @Mutation
  removeFromCart(book: BookRecord): void {
    const index = this.items.findIndex(item => item.title === book.title);
    if (index > -1) {
      const item = this.items[index];
      this.items.splice(index, 1, { ...item, qty: item.qty - 1 });
      this.items = this.items.filter(({ qty }) => qty);
    }
  }

  @Mutation
  setDiscount({ key, value }: { key: number; value: number }): void {
    this.discounts.splice(key, 1, value);
  }

}
