import Vue from 'vue';
import Vuex from 'vuex';
import CartStore from "@/store/cart.store";
import { getModule } from "vuex-module-decorators";
import BookStore from "@/store/book.store";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    book: BookStore,
    cart: CartStore
  }
});

getModule(BookStore, store);
getModule(CartStore, store);

export type State = typeof store.state;
export type Getters = typeof store.getters;
export default store;
