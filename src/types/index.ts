export interface BookRecord {
  id: string;
  title: string;
  price: number;
}

export interface CartItem extends BookRecord {
  qty: number;
}

export interface DiscountGroup {
  group: string[];
  discount?: number;
  multiplier: number;
}

export interface DiscountLevel {
  count: number;
  value: number;
  gain?: number;
  upgrades?: number[];
  downgrades?: number[]
}
